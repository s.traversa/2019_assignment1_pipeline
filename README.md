# Processo e Sviluppo Software - Assignment 1


> **COMPOSIZIONE DEL GRUPPO:** 

*  Traversa Silvia (matr. 816435)
*  Bettini Ivo Junior (matr. 806878)
*  Cocca Umberto (matr. 807191)
*  Malerba Andrea (matr. 817426)

Nella fase di commit è presente un ulteriore utente (king) creato dal pc in locale in fase di creazione della applicazione. 
Tale utente è da attribuire a i.bettini.

Link repository: [2019_assignment1_pipeline](https://gitlab.com/s.traversa/2019_assignment1_pipeline)


> **APPLICAZIONE** 

Abbiamo scelto di realizzare una webapp in Java con Maven. Questa semplice 
applicazione permette la registrazione di un utente tramite un form di login.
I dati inseriti (ossia nome e cognome) vengono salvati in un database locale realizzato con PostgreSQL.


> **CI/CD PIPELINE**

Sono stati implementati due branch, master e developer, con diversi stage di pipeline.
Nel branch **master** sono stati implementati tutti gli stage richiesti della pipeline (*build, verify, unit-test, integration-test, package, release, deploy*)
mentre nel branch **developer** sono stati implementati gli stage di *build, verify, unit-test, package e release*. 


> **STAGE DELLA PIPELINE**

**build**: Tramite il comando *mvn compile*, con le specifiche indicate nelle variabili 
*MAVEN_CLI_OPTS* e *MAVEN_OPTS*, viene compilato il progetto e scaricate eventuali dipendenze.

**verify**: Viene eseguita un'analisi statica e dinamica del codice. Vengono eseguiti in parallelo due code quality test, checkstyle e pmd.
Per entrambi i test è stato necessario scaricare i relativi plugin ed è stato
ammesso il fallimento della fase di checkstyle in modo tale da non intralciare il normale funzionamento della pipeline.

**unit-test**: Tramite JUnit viene testato il corretto funzionamento dei metodi della classe *Users*.

**integration-test**: Viene testata l'interazione fra la webapp e il database PostgreSQL ricorrendo al servizio di GitLab per la virtualizzazione di postgres, 
in particolare viene aggiunto un nuovo utente nel database e verificata poi la sua effettiva presenza in esso.

**package**: Viene creato il file .jar della webapp (escludendo i test) con l'esecuzione del comando *mvn package*.

**release**: Viene eseguito il login al Gitlab Container Registry, dopodichè l'immagine Docker
del progetto viene estratta dal registry se già esistente, costruita con il contenuto della cache e inserita nuovamente all'interno del Registry a seguito
delle modifiche.

**deploy**: Viene effettuato una connessione alla macchina virtuale traimte protocollo SSH. 
Una volta eseguita la connessione, viene scaricata l'immagine del docker dal registry della fase precedente e viene avviata l'applicazione. 