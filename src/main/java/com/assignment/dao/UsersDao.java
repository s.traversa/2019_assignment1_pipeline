package com.assignment.dao;

import org.hibernate.query.Query;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.assignment.web.model.Users;

public class UsersDao {

	public static Users getUser(int id) {
		Users a = new Users();
		
		SessionFactory factory = new Configuration().configure()
                .buildSessionFactory();
        Session session = factory.openSession(); 
        
        try{
        	@SuppressWarnings("unchecked")
		TypedQuery<Users> query = session.createQuery("FROM Users WHERE user_id="+id);
        	a = query.getSingleResult();
        }catch(Exception e) {
        	e.printStackTrace();
        }
        session.close();
        
	return a;
	}

	public static void setUser(Users u) {
						
			SessionFactory factory = new Configuration().configure()
	                .buildSessionFactory();
	        Session session = factory.openSession(); 
	        
	     
	        session.beginTransaction();
	        session.save(u);
	        session.getTransaction().commit();
	        session.close();
    
	}
}

