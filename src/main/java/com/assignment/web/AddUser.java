package com.assignment.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.assignment.web.model.Users;


/**
 * Servlet implementation class AddUser
 */
@WebServlet(urlPatterns = {"/Createuser.do"})
public class AddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		SessionFactory factory = new Configuration().configure()
                .buildSessionFactory();
        Session session = factory.openSession();        
        session.beginTransaction();
        
        Users u = new Users(request.getParameter("nome"),request.getParameter("cognome"));

        session.save(u);
        session.getTransaction().commit();
        session.close();
		
		RequestDispatcher view =
		        request.getRequestDispatcher("useradd.jsp");
		view.forward(request, response);
	}

}
