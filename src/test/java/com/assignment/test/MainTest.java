package com.assignment.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.assignment.dao.UsersDao;
import com.assignment.web.model.Users;

public class MainTest {

 @Test
 public void integrationTest() {
      Users test = new Users("Tommaso", "Paradiso");
	  UsersDao.setUser(test);
	  assertEquals(test.getNome(),"Tommaso");
	  assertEquals(test.getCognome(),"Paradiso");
 }

 @Test
 public void unitTest() {
     Users u = new Users ("Tommaso", "Paradiso");
     assertEquals(u.getNome(), "Tommaso");
     assertEquals(u.getCognome(), "Paradiso");
 }


}
